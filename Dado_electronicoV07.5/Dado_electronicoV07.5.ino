/*  Autor: Gonzalo Sacks
 *  Fecha: 3/8/2021
 *  Nombre del proyecto: Dado electronico 
 *  Descripcion: Dado electronico portable con sensor de inclinacion 
 *  Vercion: 7.5
 *  Cliente/destino: Tecnicatura en Automatizacion y Robotica UNL 
 */

int l16 = 0;//3;
int l34 = 1;//4;
int l52 = 2;//5;
int l7 = 4;//6;
int p1 = 3;//8;

bool estado = false;
bool estado_led = false;
int valor;

void setup() 
{
  pinMode(l16,OUTPUT);
  pinMode(l34,OUTPUT);
  pinMode(l52,OUTPUT);
  pinMode(l7,OUTPUT);
  pinMode(p1,INPUT); 
}

void numeros()
{
  if (valor == 1)
  {
    digitalWrite(l7,HIGH);
  }

  if (valor ==2)
  {
    digitalWrite(l52,HIGH);
  }
  
  if (valor == 3)
  {
    digitalWrite(l52,HIGH);
    digitalWrite(l7,HIGH);
  }
  
  if (valor ==4)
  {
    digitalWrite(l16,HIGH);
    digitalWrite(l52,HIGH);
  }
  
  if (valor == 5)
  {
    digitalWrite(l16,HIGH);
    digitalWrite(l52,HIGH);
    digitalWrite(l7,HIGH);
  }
  
  if (valor == 6)
  {
    digitalWrite(l16,HIGH);
    digitalWrite(l34,HIGH);
    digitalWrite(l52,HIGH);
  }
}

void apagado ()
{
  digitalWrite(l16,LOW);
  digitalWrite(l34,LOW);
  digitalWrite(l52,LOW);
  digitalWrite(l7,LOW);
}

void loop() 
{
  int pulsador = LOW;
  int pin3 = digitalRead(p1);
  randomSeed (millis());  //Inicio la semilla de la funcion random en millis()

  pulsador = filtro_antirebote_pin3(pin3);  //Aplico el filtro al pulsador/sensor
  if (pulsador == HIGH)
  {
    if ( estado == false )    //Si no estaba pulsado
    {
      estado = true;
      // cambia estado led   
      if ( estado_led == true )  //Si el led estaba prendido lo apago
      {
        estado_led = false;   
      }
      else
      {
        valor = random(1,7);  //Genera un valor aleatorio dentro de los limites
        estado_led = true;    //Si estaba apagado lo prendo
      }
    }
  }
  
  else   //Si no esta pulsado no hace nada
  {
    estado = false;
  }

//===================================================================================
  //Ejecucion de resultados 
  if (estado_led == true)
  {
    numeros(); 
  }

  else
  {
    apagado();
  }

}


//================================================================
int filtro_antirebote_pin3(int pin_pulsador)
{ 
  unsigned long tiempo_actual = millis();
  const unsigned long REBOTE_PIN = 40;

  static unsigned long tiempo_pulsado = tiempo_actual;
  static bool estado_pin_pulsador = true;

  int pulsador = LOW;
  
  // filtro del pulsador
  if (pin_pulsador == HIGH)
  {
    if (estado_pin_pulsador == false)   //Si no estaba precionado
    {
      tiempo_pulsado = tiempo_actual;
      estado_pin_pulsador = true;
    }
    else
    {
      if (tiempo_actual - REBOTE_PIN > tiempo_pulsado) //Cuento el tiempo que esa pulsado
      {
        pulsador = HIGH;     
      }
    }
  }
  else 
  {
    if (estado_pin_pulsador == true)   //Si ya estaba pulsado
    {
      tiempo_pulsado = tiempo_actual;
      estado_pin_pulsador = false;
    }
    else
    {
      if (tiempo_actual - REBOTE_PIN > tiempo_pulsado)  //Cuento el tiempo
      {
        pulsador = LOW;     
      }
    }  
  }  

  return (pulsador) ;
}
