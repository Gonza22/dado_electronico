int led = 0;


void setup() 
{
  ADMUX = (0 << REFS0) | (0 << REFS1) /* VCC used as Voltage Reference, */
        | (0 << ADLAR) /* Left Adjust Result: disabled*/
        | (0x0e << MUX0) /* Internal Reference (VBG) */; 

  ADCSRA |= (1 << ADSC) ; /* start single conversion */
  
  pinMode(led,OUTPUT);

 // Serial.begin(9600);
}

void loop() 
{
  float Vcc_value = 0 /* measured Vcc value */;
  uint16_t ADC_RES_L = 0;
  uint16_t ADC_RES_H = 0;

  
  
    if(ADCSRA & (0x01 << ADIF)) /* check if ADC conversion complete */
    {
       //digitalWrite(led,HIGH);
       ADC_RES_L = ADCL;
       ADC_RES_H = ADCH;
       Vcc_value = ( 0x400 * 1.1 ) / (ADC_RES_L + ADC_RES_H * 0x100);/* calculate the Vcc value */
       //Serial.println(Vcc_value);
       delay(400);
       ADCSRA |= (1 << ADSC) ; /* start single conversion */
       //digitalWrite(led,LOW);
       //delay(1000);
    }
  
  if(Vcc_value <= 2.5)
  {
    digitalWrite(led,HIGH);
  }

  else
  {
    digitalWrite(led,LOW);
  }
  

}
